call plug#begin('~/.nvim/plugged')

" Make sure you use single quotes
"
Plug 'mhartington/oceanic-next'
Plug 'morhetz/gruvbox'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'lervag/vimtex'
Plug 'w0rp/ale'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Plug 'neomake/neomake'

" Add plugins to &runtimepath
call plug#end()

" Theme ----------------------------------------------------------------------
set background=dark

" "" For Neovim 0.1.3 and 0.1.4
" let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
" let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 0
" 
" " Or if you have Neovim >= 0.1.5
" if (has("termguicolors"))
"  set termguicolors
" endif
" 
syntax enable
" colorscheme OceanicNext
colorscheme gruvbox

" ----------------------------------------------------------------------------

filetype indent plugin on

" ALE linter
let g:ale_python_pylint_executable = 'flake8'

" Enable completion where available.
" let g:ale_completion_enabled = 1

" Use deoplete.
let g:deoplete#enable_at_startup = 1

" deoplete: close window after completion
autocmd CompleteDone * silent! pclose!

" airline --------------------------------------------------------------------

set laststatus=2
" let g:airline_theme             = 'gruvbox'
let g:airline_theme             = 'molokai'
let g:airline_enable_branch     = 1
let g:airline_powerline_fonts = 1
set encoding=utf-8

" ----------------------------------------------------------------------------

" Time Stamps with F6
nnoremap <F6> "=strftime("%c")<CR>P
inoremap <F6> <C-R>=strftime("%c")<CR>

" Numbers for lines
set number

" Trailing whitespaces
nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR> 

" Use spaces
set expandtab
" make *tab* insert indents instead of tabs at the beginning of a line
" set smarttab
" size of a hard tabstop
set tabstop=4
set softtabstop=4
" Size of an *indent*
set shiftwidth=4

" Set directory to the location of the edited file
set autochdir

" Highlight characters beyond the 79th position in a line
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%80v.\+/

" OOMMFs .mif files syntax colouring as a .tcl file
au BufRead,BufNewFile *.mif set filetype=tcl
" MuMax3 files with Go syntax
au BufRead,BufNewFile *.mx3 set filetype=go

" Guide lines for identation
" let g:indent_guides_guide_size = 1

" Underline for spell
hi clear SpellBad
hi SpellBad cterm=underline

" Fill the rest of a line with characters ------------------------------------
function! FillLine( str )
    " set tw to the desired total length
    let tw = &textwidth
    if tw==0 | let tw = 79 | endif
    " echo tw
    " strip trailing spaces first
    .s/[[:space:]]*$//
    " calculate total number of 'str's to insert
    let reps = (tw - col("$")) / len(a:str)
    " insert them, if there's room, removing trailing spaces (though forcing
    " there to be one)
    " echo reps
    if reps > 0
        .s/$/\=(' '.repeat(a:str, reps))/
    endif
endfunction

" Assign a map to this function
map <F3> :call FillLine( '-' )

" ----------------------------------------------------------------------------

" Format paragraphs (ip --> internal paragraph) using Control Q
" (it seems that C-Q is the equivalent to C-V, so redefine it)
nnoremap <C-Q> gqip

" Paste from the computer clipboard
noremap <LocalLeader>p "+p
" Yank to the computer clipboard
vnoremap <LocalLeader>y "+y

" Insert blank line in visual mode with Enter and Shift-Enter
nnoremap <Enter> :call append(line('.'), '')<CR>
nnoremap <S-Enter> :call append(line('.')-1, '')<CR>

" Disable arrow keys (ugh!)
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" Riv (for RST files): avoid folding
" let g:riv_disable_folding = 1
